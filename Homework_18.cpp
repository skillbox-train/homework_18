#include <iostream>
#include <vector>
#include <string>
using namespace std;
template<typename K>

class Stack
{
public: 
    void push(K);
    K pop();
    void show();
private:
    vector<K> v;
};


int main()
{
    int N;
    std::cout << "Enter integer number to POP" << "\n";
    std::cin >> N;
    std::cout << "\n";

    string Baby;
    std::cout << "Enter word to POP" << "\n";
    std::cin >> Baby;
    std::cout << "\n";

    Stack <int> a;
    a.push(1); a.push(2); a.push(3); a.push(N);
    a.show();
    std::cout << "Pop: " << a.pop() << "\n";
    a.show();
    std::cout << "\n";

    Stack <string> b;
    b.push("Uno"); b.push("Dos"); b.push("Tres"); b.push(Baby);
    b.show();
    std::cout << "Pop: " << b.pop() << "\n";
    b.show();

    return 0;
}

template <class K> void Stack <K>::push(K elem)
{
    v.push_back(elem);
}

template <class K> K Stack <K>::pop()
{
    K elem = v.back();
    v.pop_back();
    return elem;
}
template <class K> void Stack <K>::show()
{
    std::cout << "Stack:";
    for (auto e : v) cout << e << " ";
    std::cout << "\n";
}